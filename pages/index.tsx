import Head from 'next/head';

import styles from "../src/styles/pages/Home.module.css";

export default function Home() {
  return (
    <>
    <div className={styles.container}>
      <Head>
        <title>Falamed - Sua saúde do seu jeito</title>
      </Head>
      <header>
        <a href="/">
        <img src="/logotipo.svg" alt="Logotipo" />
        </a>
        <a href="/login">
        <button type="button" >
            Login
        </button>
        </a>
      </header>
      <section>
          <strong>Bem vindo à falamed</strong>
          <span>Descubra grátis como cuidar da sua saúde em menos de 5 minutos</span>
          <a href="/signup">
          <button type="button">
            Faça já o teste
          </button>
          </a>
      </section>
    </div>
    <div className={styles.containerBg}>
      <div className={styles.container}>
        <div className={styles.features}>
          <div>
          <strong>O cuidado com a saúde inicia com a prevenção de doenças</strong>
          <span>Comece descobrindo quais recomendações estão indicadas para você</span>
          </div>
          <div>
          <img src="mascot.svg" alt="Mascote" />
          </div>
          </div>
      </div>
    </div>
    <footer className={styles.containerFooter}>
      <div>
      <img src="logotipo.svg" alt="Logotipo" />
      <p>Falamed serviços de saúde ltda</p>
      <p>39.837.361/0001-22</p>
      <p>Av. Paulista, 171 - São Paulo - SP</p>
      </div>
      <div>
        <strong>Mapa do site:</strong>
        <a href="/">Página inicial</a>
        <a href="/login">Login</a>
        <a href="/signup">Faça nosso teste</a>
      </div>
    </footer>
    </>
  )
}
